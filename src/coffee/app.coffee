+ do ($ = jQuery, window) ->
    'use strict';
    $ ->
        owl = $('.items-carousel')
        item = $('.category-carousel .item')
        selectTitle = $('.js-select-title')
        selectLink = $('.js-select-link')
        spinner = $('#spinner')
        time = 200

        item.on 'tap', (e) ->
            e.preventDefault()

            target = $(@).data 'target'
            link = $(@).data 'link'
            title = $(@).data 'title'


            item.removeClass 'active'

            $(@)
                .addClass 'active'

            selectTitle.text(title)
            selectLink.attr 'href', link

            $(target)
                .addClass 'show'
                .siblings '.panel'
                .removeClass 'show'

            owl.on 'refresh.owl.carousel', (e) ->
                spinner.show()

                setTimeout( ->
                        spinner.hide()
                    time)
                return

            owl.on 'refreshed.owl.carousel', (e) ->
                carousel = $(@)
                owl.removeClass 'active'
                setTimeout( ->
                        carousel.addClass 'active'
                        spinner.hide()
                        return
                    time)
                return
            return
        return